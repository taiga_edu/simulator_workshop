# Installation instructions for PyBullet

## Install Visual Studio

1. Go to www.visualstudio.microsoft.com/downloads

2. Select the `Community` edition

   ![vs-1](images/vs-1.jpg)

   

3. When downloaded, run the executable. Allow it to install, and proceed until you see the next screen.

4. Select `Desktop Development with C++`

   ![vs-2](images/vs-2.jpg)

   

   1. Once installed, you may choose **not to register** (`Not now, maybe later.`) when prompted

      <img src="images/vs-4.jpg" alt="vs-4" style="zoom: 70%;" />

   

5. Complete installation steps as required. Once visual studio launches, you're ready to go.

![vs-7](images/vs-7.jpg)



## Install Python.

Install python by your preferred method.

1. Download the installer from https://www.python.org/downloads, select `Downlaod Python 3.8.3` or the latest version. There should be a big yellow button near the top of the page `Download Python 3`

   <img src="images/python-site.jpg" alt="python-site" style="zoom:70%;" />

   

2. Run the installer.

3. Select `Add Python 3.8 to Path` and **choose the second install option** `Customize installation`

   ![py2](images/py2.jpg)



4. Select all options on the next page

   ![py3](images/py3.jpg)

   

5. Change the installation directory to `C:\Python\Python####` where `####` is anything that was there before.

   ![py5](images/py5.jpg)

6. Install.



## Install PyBullet

### Open a CMD window

To open a cmd window and run the commands below, press the windows key, and type `cmd`.

The search results should show something like:

<img src="images/cmd-1.jpg" alt="cmd-1" style="zoom:70%;" />

Select the  `Command Prompt` app.

### Install PyBullet

To install PyBullet, run the following command in a terminal:

`pip install pybullet`

![2](images/2.jpg)



It will download the package.

![3](images/3.jpg)



And install it.

![4](images/4.jpg)



When completed it should look similar to this (please ignore the warning).

![5-ignore-warning](images/5-ignore-warning.jpg)



## Install numpy

Use `pip install numpy` to install the `numpy` library.

![8](images/8.jpg)



## Install pysdl2 for support of xbox controllers and other gamepads

Use `pip install pysdl2`

The output should appear familiar at this point.

> **__NOTE:__** **On Windows** also install: `pip install pysdl2-dll`



# Running Examples

To run an example, in the `cmd` prompt navigate to the `scripts` directory.

![run-1](images/run-1.jpg)



Then, run an example with: `python filename.py` ( for instance `python main_drive.py`).

![run-2](images/run-2.jpg)



Which should open a simulation.

![run-3](images/run-3.jpg)