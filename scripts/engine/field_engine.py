
#!/usr/bin/env python3
# This Python file uses the following encoding: utf-8

import time
import numpy as np
import pybullet as p
import pybullet_data
from engine.joystick import Joystick


class PyBulletEngineField():

    def __init__(self, rate=100.0, realtime = 0):

        self.rate = rate  # run at 100Hz
        self.dt = 1.0/self.rate
        self.is_ready = False
        self.realtime = realtime

    # build the simple robot by default
    def setup(self, student_function, robot="../robots/2dof/robot_rr.urdf", position =[0, 0, 0], joints=[0,1], mode=2, fixed=False):
        # client type, GUI, DIRECT (non-graphical: shared memory, udp, tcp)
        if(not joints):
            print("ERROR: please specify the joints to control")
            exit(1)
        self.joints = joints
        self.mode=mode
        self.student_function = student_function

        self.physicsClient = p.connect(p.GUI)

        p.setAdditionalSearchPath(pybullet_data.getDataPath())
        p.configureDebugVisualizer(p.COV_ENABLE_GUI, 0)
        p.configureDebugVisualizer(p.COV_ENABLE_SEGMENTATION_MARK_PREVIEW, 0)
        p.configureDebugVisualizer(p.COV_ENABLE_DEPTH_BUFFER_PREVIEW, 0)
        p.configureDebugVisualizer(p.COV_ENABLE_RGB_BUFFER_PREVIEW, 0)

        p.setTimeStep(self.dt)
        p.setRealTimeSimulation(self.realtime)
        p.setGravity(0, 0, -9.81)

        self.planeId = p.loadURDF("plane.urdf")
        start_pos = [0,0,0] #position . . . x, y, z is up
        start_orientation = p.getQuaternionFromEuler([0,0,0]) #get quaternion q=(x,y,z,w) ASK and I will explain...
        fieldId = p.loadURDF("../models/field_small.urdf",start_pos, start_orientation, useFixedBase=1)

        start_pos = position
        print("POSITION: ", start_pos)
        start_orientation = p.getQuaternionFromEuler([0, 0, 0])
        self.robot = p.loadURDF(robot,start_pos, start_orientation, useFixedBase=fixed)


        visualShapeId = p.createVisualShape(shapeType=p.GEOM_MESH,fileName="../models/post_centre_small.obj",rgbaColor=[1,1,1,1], specularColor=[0.4,.4,0])
        collisionShapeId = p.createCollisionShape(shapeType=p.GEOM_MESH, fileName="../models/post_centre_small.obj")

        for i in [-1, 0, 1]:
            for j in [-1, 0 ,1]:
                # if i==0 and j==0:
                #     continue
                print(i, ", ", j)
                p.createMultiBody(baseMass=1,baseCollisionShapeIndex=collisionShapeId, baseVisualShapeIndex = visualShapeId, basePosition = [1.5*i,1.5*j,0.01])


        self.num_joints = len(self.joints)

        #set friction for torque mode
        if self.mode==0:
            p.setJointMotorControlArray(self.robot,self.joints, p.VELOCITY_CONTROL, forces=0.1*np.ones(self.num_joints))

        self.joystick = Joystick()
        self.is_ready = True
        return True

    def get_joint_states(self):
        if not self.is_ready:
            print("WARNING - reading joints when the simulation is not ready")
            return
        joint_states = p.getJointStates(self.robot, self.joints)
        joint_pos = [state[0] for state in joint_states]
        joint_vel = [state[1] for state in joint_states]
        joint_eff = [state[3] for state in joint_states]
        return joint_pos, joint_vel, joint_eff

    def update(self):
        if not self.is_ready:
            print("WARNING - calling update() when the simulation is not ready")
            return

        self.joystick.update()
        pos, vel, acc = self.get_joint_states()
        cmd = self.student_function(pos, vel, acc, self.joystick)
        if self.mode==0:
            p.setJointMotorControlArray(self.robot,self.joints, p.TORQUE_CONTROL, forces=cmd)
        elif self.mode==1:
            print("WARNING: unimplemented")
        elif self.mode==2:
            p.setJointMotorControlArray(self.robot, self.joints, p.POSITION_CONTROL, targetPositions=cmd)
        p.stepSimulation()
    
    def spin(self):
        while True:
            self.update()
            time.sleep(self.dt) #not really that accurate, but close enough