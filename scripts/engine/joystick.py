import ctypes
import sdl2


class Joystick:
    def __init__(self):
        sdl2.SDL_Init(sdl2.SDL_INIT_JOYSTICK)
        self.axis = {0:0., 1:0., 2:0., 3:0., 4:0., 5:0.}
        self.button = {0:False, 1:False, 2:False, 3:False}

    def update(self):
        event = sdl2.SDL_Event()
        while sdl2.SDL_PollEvent(ctypes.byref(event)) != 0:
            if event.type == sdl2.SDL_JOYDEVICEADDED:
                self.device = sdl2.SDL_JoystickOpen(event.jdevice.which)
            elif event.type == sdl2.SDL_JOYAXISMOTION:
                self.axis[event.jaxis.axis] = event.jaxis.value/32767.0
            elif event.type == sdl2.SDL_JOYBUTTONDOWN:
                self.button[event.jbutton.button] = True
            elif event.type == sdl2.SDL_JOYBUTTONUP:
                self.button[event.jbutton.button] = False
