#!/usr/bin/env python3
# This Python file uses the following encoding: utf-8

from engine.joystick import Joystick
from engine.simple_engine import PyBulletEngineSimple
from engine.field_engine import PyBulletEngineField # try using this environment as well.
from student_drive import student_function

if __name__ == "__main__":

    robot_engine = PyBulletEngineSimple()
    # robot_engine = PyBulletEngineField() # try using this environment as well. (comment the line above, and uncomment this)
    robot_engine.setup(student_function,
                        "../robots/jackal/jackal_description/urdf/jackal.basic.urdf",
                        position=[0,0,2], joints=[1,2,3,4], mode=0)
    robot_engine.spin()
