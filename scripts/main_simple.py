#!/usr/bin/env python3
# This Python file uses the following encoding: utf-8

from engine.joystick import Joystick
from engine.simple_engine import PyBulletEngineSimple

from student_simple import student_function

if __name__ == "__main__":

    robot_engine = PyBulletEngineSimple()
    robot_engine.setup(student_function, fixed = True)
    robot_engine.spin()
