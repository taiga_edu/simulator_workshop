# This Python file uses the following encoding: utf-8

import numpy as np

def student_function(position, velocity, acceleration, joystick):
    # print("student function")
    # print("joy axis: ", joystick.axis)
    # print("joy buttons: ", joystick.button)

    num_joints = len(position)

    throttle_force = np.ones(num_joints)*joystick.axis[1]*2.0
    steer_force = np.array([1., -1., 1., -1.])*joystick.axis[0]*2.0
    cmd_forces = throttle_force + steer_force

    return cmd_forces
